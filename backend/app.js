// Root imports
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

// Configurations
const serverConfig = require('./config/server.json');

const app = express()
    .use(cors())
    .use(bodyParser.json());

// MongoDB
let mongo = require('./db/mongo');
mongo.connect();

// Routes
app.use('/ping', require('./routes/ping'));
app.use('/roles', require('./routes/roles'));
app.use('/blog-posts', require('./routes/blog-posts'));
app.use('/auth', require('./routes/auth/auth-base'));
app.use('/email', require('./routes/email'));

// Listen
app.listen(serverConfig.port, () => {

    console.log("    ")
    console.log("    ")
    console.log(`Backend is listening for requests on port ${serverConfig.port}.`);
    console.log("    ")

})