const Role = require('../models/role.model');
const express = require('express');
const router = express.Router();

// GET /roles
// Reponse: Array of all roles
router.get('/', (req, res) => {
    Role.find({}, (err, roleArray) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(roleArray);
    })
})

// GET /roles/:id
// Response: Requested role document
router.get('/:id', (req, res) => {
    Role.findOne({ _id: req.params.id }, (err, role) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(role);
    })
})

// DELETE /roles/:id
// Response: Deleted document
router.delete('/:id', (req, res) => {
    Role.findByIdAndDelete(req.params.id, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(doc);
    })
})

// POST /roles/
// Reponse: Document of role created
router.post('/', (req, res) => {
    
    let role = new Role(req.body);
    role.save()
        .then((doc) => res.status(200).json(doc), (reason) => res.status(500).send(reason));

})

// PATCH /roles/:id
router.patch('/:id', (req, res) => {
    
    Role.findByIdAndUpdate(req.params.id, req.body, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    });
})

// POST /roles/:id/perm
// Reponse: New document
router.post('/:id/perm', (req, res) => {
    
    let perm = req.body.permissions;
    if (!perm) {
        return res.sendStatus(400);
    }

    Role.findByIdAndUpdate(req.params.id, {$push: { "permissions": perm }}, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(doc);
    });
})

// DELETE /roles/:id/perm
// Response: Deleted document
router.delete('/:id/perm', (req, res) => {
    
    let perm = req.body.permissions;
    if (!perm) {
        return res.sendStatus(400);
    }

    Role.findByIdAndUpdate(req.params.id, {$pullAll: { "permissions": perm }}, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(doc);
    });
})

module.exports = router;