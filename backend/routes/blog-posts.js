const express = require('express');
const router = express.Router();

const BlogPost = require('../models/blog-post.model');

// POST /blog-posts/
// Response: New document
router.post('/', (req, res) => {
    let blogPost = new BlogPost(req.body);
    blogPost.save()
        .catch((err) => {
            console.error(err);
            return res.status(500).send(err);
        }).then((doc) => {
            return res.status(200).json(doc);
        });
});

// DELETE /blog-posts/post/:id
// Reponse: Deleted document
router.delete('/post/:id', (req, res) => {
    BlogPost.findByIdAndDelete(req.params.id, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        return res.status(200).json(doc);
    })
});

// GET /blog-posts/post/:id
// Reponse: Requested document
router.get('/post/:id', (req, res) => {
    BlogPost.findById(req.params.id, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        return res.status(200).json(doc);
    })
});

// PATCH /blog-posts/post/:id
router.patch('/post/:id', (req, res) => {
    BlogPost.findByIdAndUpdate(req.params.id, { $set: req.body }, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    })
});

// PATCH /blog-posts/post/:id/incr/likes
// Increment likes of a blog-post
router.patch('/post/:id/incr/likes', (req, res) => {

    BlogPost.findByIdAndUpdate(req.params.id, { $inc: { likes: 1 } }, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    });
});

// PATCH /blog-posts/post/:id/incr/views
// Increment views of a blog-post
router.patch('/post/:id/incr/views', (req, res) => {

    BlogPost.findByIdAndUpdate(req.params.id, { $inc: { views: 1 } }, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    });
});

// PATCH /blog-posts/post/:id/decr/likes
// Decrement likes of a blog-post
router.patch('/post/:id/decr/likes', (req, res) => {

    BlogPost.findByIdAndUpdate(req.params.id, { $inc: { likes: -1 } }, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    });
});

// GET /blog-posts/list/:page
// Response: An array of blog posts from the requested page
router.get('/list/:page', (req, res) => {
    BlogPost.find((err, blogPosts) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        let pageNumber = req.params.page - 1;
        let pageItemStart = (pageNumber * 10);
        let pageItemEnd = pageItemStart + 10;
        
        if (blogPosts.length < pageItemStart + 1) {
            return res.sendStatus(404);
        }

        let newArray = blogPosts.slice(pageItemStart, pageItemEnd);

        res.status(200).json(newArray);
    });

})

// GET /blog-posts/list/:page
// Response: An array of all blog posts
router.get('/list/', (req, res) => {
    BlogPost.find((err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        return res.status(200).json(doc);
    })
})

module.exports = router;