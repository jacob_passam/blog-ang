const express = require('express');
const router = express.Router();

router.use('/mgmt', require('./auth-mgmt'));
router.use('/jwt', require('./auth-jwt'));

module.exports = router;