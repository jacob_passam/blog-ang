const express = require('express');
const router = express.Router();

const User = require('../../models/user.model');

const jwt = require('jsonwebtoken');
const sha256 = require('sha256');
const RefreshToken = require('../../models/refresh-token.model');

const secrets = require('../../config/secret.json');

const ACCESS_TOKEN_EXPIRY = '10s';

const validateJwtAccess = (req, res, next) => {
    let authHeader = req.headers.authorization;

    if (authHeader) {
        
        // Access token
        let accessToken  = authHeader.split(' ')[1];
        
        // Verify access token
        jwt.verify(accessToken, secrets.accessSecret, (err, user) => {
            if (err) {
                // access token is invalid/expired or some other server error
                return res.status(403).send((err.name) ? err.name : "");
            }

            // set user obj, next handler
            req.user = user;
            next();

        });
    } else {
        res.sendStatus(401);
    }
}

// GET /auth/jwt/verify
// Response: 200
router.get('/verify', validateJwtAccess, (req, res) => {
    res.sendStatus(200);
})

// POST /auth/jwt/login
// Response: User ID and access token
router.post('/login', (req, res) => {
    let { emailAddress, password } = req.body;
    password = sha256(password);

    // Find user via email
    User.findOne({ emailAddress }, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        if (!doc) {
            return res.status(401).send("Email or password incorrect");
        } else {
            if (doc.password != password) {
                return res.status(401).send("Email or password incorrect");
            }

            // Genereate access tokens and refresh tokens.
            let accessToken = jwt.sign({ userId: doc._id }, secrets.accessSecret, { expiresIn: ACCESS_TOKEN_EXPIRY });
            let refreshToken = jwt.sign({ userId: doc._id }, secrets.refreshSecret);

            // Delete refresh token if exists
            RefreshToken.findOneAndDelete({ userId: doc._id }, (err) => {
                if (err) {
                    console.error(err);
                    return res.status(500).send(err);
                }
            }).then(() => {

                // Save new refresh token.
                new RefreshToken({ userId: doc._id, token: refreshToken }).save((err) => {
                    if (err) {
                        console.error(err);
                        return res.status(500).send(err);
                    }

                    console.log("saved to db1!!!");
                    let userId = doc._id;
                    return res.status(200).json({ userId, accessToken, refreshToken });
                });

            })

        }
    })

});

// POST /auth/jwt/token
// Response: New access token
router.post('/token', (req, res) => {
    let { userId, token } = req.body;

    // Have they provided a user id & refresh token in body?
    if (!userId || !token) {
        return res.sendStatus(400);
    }

    // Is the refresh token valid?
    RefreshToken.findOne({ userId, token }, (err, doc) => {
        if (err) {
            console.log(err);
            return res.status(500).send(err);
        }

        if (!doc) {
            // Invalid refresh token
            return res.status(403).send("Unauthorized refresh token provided")
        }

        let newAccessToken = jwt.sign({ userId: doc._id }, secrets.accessSecret, { expiresIn: ACCESS_TOKEN_EXPIRY });

        return res.status(200).json({ accessToken: newAccessToken });
    });
});

// POST /auth/jwt/logout
// Response: Logged out user document
router.post('/logout', validateJwtAccess, (req, res) => {

    /* No need to validate in here: validateJwt should ensure tokens work and give us a valid userId.
       All we need to do now is remove the refresh token and send a 200.*/

    let userId = req.user.userId;
    console.log("received logout req")

    RefreshToken.findOneAndDelete({ userId }, (err, doc) => {
        if (err) {
            console.log(err);
            return res.status(500).send(err);
        }

        res.status(200).json(doc);
    });
})

module.exports = router;
