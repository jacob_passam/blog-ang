const express = require('express');
const router = express.Router();

const sha256 = require('sha256');

const User = require('../../models/user.model');

// GET /auth/mgmt/list/
// Reponse: Array of all users
router.get('/list/', (req, res) => {
    User.find({})
        .then(docs => res.status(200).json(docs))
        .catch(reject => { console.error(reject); res.status(500).send(reject); } );
})

// GET /auth/mgmt/list/:page
// Response: Paginated user list
router.get('/list/:page', (req, res) => {
    User.find({}, (err, docs) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        let pageNumber = req.params.page - 1;
        let pageItemStart = (pageNumber * 10);
        let pageItemEnd = pageItemStart + 10;
        
        if (docs.length < pageItemStart + 1) {
            return res.sendStatus(404);
        }

        let newArray = docs.slice(pageItemStart, pageItemEnd);

        res.status(200).json(newArray);

    });
})

// POST /auth/mgmt/ CREATE USER
// Reponse: Created user document
router.post('/', (req, res) => {
    let user = new User(req.body);

    User.findOne({ emailAddress: user.emailAddress }, (err, doc) => {
        if (err) {
            console.log(err);
            return res.status(500).send(err);
        }

        if (!doc) {
            user.password = sha256(user.password);
            user.save()
                .catch(reject => { console.error(reject); res.status(500).send(reject); } )
                .then(doc => res.status(200).json(doc));
        } else {
            return res.status(409).send('E-mail address already taken');    
        }
    })
})

// GET /auth/mgmt/user/:user
// Response: Requested user document
router.get('/user/:user', (req, res) => {
    User.findOne({ _id: req.params.user }, (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(doc);
    });
})

// PATCH /auth/mgmt/user/:user
router.patch('/user/:user', (req, res) => {
    if (req.body.password) {
        req.body.password = sha256(req.body.password);
    }

    User.findOneAndUpdate({ _id: req.params.user }, {$set: req.body}, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    })
})

// PATCH /auth/mgmt/user/:user/add-role/:role
router.patch('/user/:user/add-role/:role', (req, res) => {
    User.findOneAndUpdate({ _id: req.params.user }, {$push: { roles: req.params.role }}, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    })
});

// PATCH /auth/mgmt/user/:user/remove-role/:role
router.patch('/user/:user/remove-role/:role', (req, res) => {
    User.findOneAndUpdate({ _id: req.params.user }, {$pull: { roles: req.params.role }}, (err) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.sendStatus(200);
    })
});

// DELETE /auth/mgmt/user/:user DELETE USER
// Response: Deleted user document
router.delete('/user/:id/', (req, res) => {
    User.findOneAndDelete({ _id: req.params.id } , (err, doc) => {
        if (err) {
            console.error(err);
            return res.status(500).send(err);
        }

        res.status(200).json(doc);
    })
});

module.exports = router;