const express = require('express');
const router = express.Router();

// GET /ping
router.get('/', (req, res) => {
    res.status(200).type('text').send("I am running (and not self-aware), I promise.");
});


module.exports = router;