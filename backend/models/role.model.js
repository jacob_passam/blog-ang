const mongoose = require('mongoose');

let roleSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },

    permissions: {
        type: Array,
        default: []
    }
});

module.exports = mongoose.model("role", roleSchema, "roles");