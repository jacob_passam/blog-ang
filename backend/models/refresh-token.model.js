const mongoose = require('mongoose');

let tokenSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },

    token: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("refresh_token", tokenSchema, "refresh_tokens");