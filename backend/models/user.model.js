const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },

    lastName: {
        type: String,
        required: true
    },

    dateOfBirth: {
        type: Date,
        required: true
    },

    emailAddress: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    // ROLE IDS
    roles: {
        type: Array,
        default: []
    }
});

module.exports = mongoose.model("user", userSchema, "users");