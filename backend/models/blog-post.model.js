const mongoose = require('mongoose');

let blogPostSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },

    content: {
        type: String,
        required: true
    },

    thumbnailUrl: {
        type: String,
        required: true
    },

    dateCreated: {
        type: Date,
        required: true
    },

    likes: {
        type: Number,
        default: 0
    },

    views: {
        type: Number,
        default: 0
    },

    comments: {
        type: Array,
        default: []
    }

});

module.exports = mongoose.model("blog_post", blogPostSchema, "blog_posts");