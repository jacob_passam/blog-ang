const mongoose = require('mongoose');
const mongoConfig = require('../config/mongo.json');

function connect() {
    mongoose.connect(
        `mongodb://${mongoConfig.username}:${mongoConfig.password}@${mongoConfig.host}:${mongoConfig.port}/${mongoConfig.database}`).catch((err) => {
        console.log("Error: MongoDB has encountered an error whilst connecting")
        console.log(err);
    }).then(() => {
        console.log("Success: MongoDB has connected")
    });
    
}

module.exports = { connect };

