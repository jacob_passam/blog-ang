import { Injectable } from '@angular/core';
import { RequiredValidator } from '@angular/forms';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from 'src/app/modules/auth/auth-service/auth.service';
import { HttpRequestService } from 'src/app/services/http-request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    
      if (!this.authService.isLoggedIn()) {
        this.router.navigate(['auth', 'login']);
        return false;
      }

      return true;

  }
}
