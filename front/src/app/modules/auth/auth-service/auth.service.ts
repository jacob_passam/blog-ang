import { Injectable } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, } from 'rxjs/operators';
import { HttpErrorResponse, HttpRequest, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { error } from '@angular/compiler/src/util';
import { Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpRequestService: HttpRequestService, private router: Router) { }

  login(email: string, password: string): Promise<string> {

    return new Promise((resolve, reject) => {
      this.httpRequestService.login(email, password).subscribe((response) => {
      
        this.createSession({ userId: response.body.userId, accessToken: response.body.accessToken, refreshToken: response.body.refreshToken });
  
        resolve("SUCCESS");
  
      }, (err: HttpErrorResponse) => {
        
        if (err.status == 401) resolve("Incorrect e-mail or password.");
        else resolve(err.statusText);
        
      });
    });
  }

  isLoggedIn(): boolean {
    return (this.getSession().userId != null && this.getSession().accessToken != null && this.getSession().refreshToken != null);
  }

  register(firstName: string, lastName: string, dateOfBirth: Date, email: string, password: string): Promise<string> {

    if (this.getSession().accessToken) return new Promise((resolve) => resolve("Already logged in."));
``
    return new Promise(resolve => { 
      this.httpRequestService.register(firstName, lastName, dateOfBirth, email, password).subscribe(response => {
      
        // SUCCESSFUL REGISTRATION
  
        this.login(email, password);

        resolve("SUCCESS");
  
      }, (error: HttpErrorResponse) => {
        
        if (error.status == 409) resolve("E-mail address has already been taken.")
        else resolve(error.statusText);

      })
    }); 
  }

  grabNewToken(): Observable<HttpResponse<any>> {
    return this.httpRequestService.grabNewToken(this.getSession().userId, this.getSession().refreshToken);
  }

  logout() {
    this.httpRequestService.logout().toPromise().finally(() => this.killSession());
  }

  createSession(sessionInfo: SessionInfo) {
    localStorage.setItem('user-id', sessionInfo.userId);
    localStorage.setItem('access-token', sessionInfo.accessToken);
    localStorage.setItem('refresh-token', sessionInfo.refreshToken);
  }

  killSession() {
    localStorage.removeItem('user-id');
    localStorage.removeItem('access-token');
    localStorage.removeItem('refresh-token');
  }

  getSession(): SessionInfo {
    return {
      userId: localStorage.getItem('user-id'),
      accessToken: localStorage.getItem('access-token'),
      refreshToken: localStorage.getItem('refresh-token')
    };
  }

  validateAccess(): Promise<boolean> {
    return new Promise((resolve) => {

      this.httpRequestService.validateAccess().subscribe(
        (res: HttpResponseBase) => {
          if (res.status == 200) resolve(true);
          else return resolve(false);
        }, (err: HttpErrorResponse) => {
          return resolve(false);
        }
      );

    })
  }
}

export interface SessionInfo {
  userId: string,
  accessToken: string,
  refreshToken: string;
}