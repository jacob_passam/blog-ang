import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponseBase, HttpErrorResponse
} from '@angular/common/http';
import { AuthService } from '../auth-service/auth.service';

import { Observable, throwError } from 'rxjs';
import { retry, catchError, tap, finalize } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/services/http-request.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log(request.url);
    request = this.addToken(request);

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        
        // JWT Token Expired
        if (err.error == "TokenExpiredError") {
          this.authService.grabNewToken().subscribe((res) => {

            console.log("Got new token!");  
            localStorage.setItem('access-token', res.body.accessToken);
            request = this.addToken(request.clone());

            return next.handle(request);
          }, (error) => {
            console.log("Received error whilst grabbing token.");
            return throwError(error);
          });        
        }

        return throwError(err);
      })
    );
  }

  addToken(request: HttpRequest<unknown>) {
    let token: string = this.authService.getSession().accessToken;

    // Add token to request (if it exists)
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      })
    }

    return request;
  }
}
