import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorMessage: string[];

  loginForm: FormGroup;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['']);
    }

    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required])
    });
  }

  submitLoginForm() {
    this.errorMessage = [];

    if (!this.loginForm.touched) {
      this.errorMessage.push("Please enter your credentials.")
    } else {
      
      if (!(this.loginForm.get('email').value == "")) {
        this.errorMessage.push("You have not entered an email address.")
      }

      if (!(this.loginForm.get('password').value == "")) {
        this.errorMessage.push("You have not entered a password.")
      }

      if (!this.loginForm.get('email').valid) {
        this.errorMessage.push("That email address is invalid.")
      }
    }

    if (this.loginForm.valid) {

      this.authService.login(this.loginForm.get('email').value, this.loginForm.get('password').value).then(
        (res) => {
          if (res != "SUCCESS") {
            this.errorMessage.push(res);
          } else {
            // Successfully logged in
            this.router.navigate(['']);
          }
        }
      )

    }

  }

}
