import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { AuthService } from '../../auth/auth-service/auth.service';

@Component({
  selector: 'app-default-page',
  templateUrl: './default-page.component.html',
  styleUrls: ['./default-page.component.scss']
})
export class DefaultPageComponent implements OnInit {

  constructor(public authService: AuthService, private httpRequestService: HttpRequestService) { }

  ngOnInit(): void {
  }

}
