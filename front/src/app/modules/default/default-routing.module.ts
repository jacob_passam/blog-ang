import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultPageComponent } from './default-page/default-page.component';
import { AuthGuardService } from '../../util/auth-guard/auth-guard.service';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: DefaultPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefaultRoutingModule { }
