import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './util/page-not-found/page-not-found.component';

const routes: Routes = [
  
  // LAZY LOADING
  { path: '', pathMatch: 'full', loadChildren: () => import('./modules/default/default.module').then(m => m.DefaultModule) },
  
  { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule) },
  
  // 404
  { path: '**', pathMatch: 'full', component: PageNotFoundComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
