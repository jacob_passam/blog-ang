import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  API_BASE: string = "http://localhost:2917";

  constructor(private httpClient: HttpClient) { }

  // Type 'text' should use HttpResponse<any>
  // Type 'json' should use HttpResponse<Response>

  pingApi(): Observable<HttpResponse<any>> {
    return this.httpClient.get(`${this.API_BASE}/ping`, { observe: 'response', responseType: 'text'});
  }

  login(emailAddress: string, password: string): Observable<HttpResponse<any>> {
    return this.httpClient.post(`${this.API_BASE}/auth/jwt/login`, { emailAddress, password }, { observe: 'response'});
  }

  register(firstName: string, lastName: string, dateOfBirth: Date, emailAddress: string, password: string): Observable<HttpResponse<any>> {
    return this.httpClient.post(`${this.API_BASE}/auth/mgmt/`, { firstName, lastName, dateOfBirth, emailAddress, password }, { observe: 'response'});
  }

  validateAccess(): Observable<HttpResponse<any>> {
    return this.httpClient.get(`${this.API_BASE}/auth/jwt/verify`, { observe: 'response', responseType: 'text'});
  }

  logout(): Observable<HttpResponse<any>> {
    return this.httpClient.post(`${this.API_BASE}/auth/jwt/logout`, { }, { observe: 'response'});
  }

  grabNewToken(userId: string, token: string): Observable<HttpResponse<any>> {
    return this.httpClient.post(`${this.API_BASE}/auth/jwt/token`, { userId, token }, { observe: 'response', responseType: 'json'});
  }

}
